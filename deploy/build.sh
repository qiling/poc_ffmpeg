#!/usr/bin/env bash

ROOT="$(cd "$(dirname "$0")/.." && pwd)"

TAG=`date +"%Y%m%d%H%M%S"`
# TAG=test_pub

IMAGE_TAG=10.129.35.207:5000/ffmpeg/poc:$TAG
docker build --no-cache -t $IMAGE_TAG -f deploy/Dockerfile $ROOT

# docker build --no-cache -t $IMAGE_TAG -f simple_brtc_robot/deploy/Dockerfile $ROOT && \
#    docker push $IMAGE_TAG

