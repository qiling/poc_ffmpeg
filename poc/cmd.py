import ffmpeg
import json
import os
import unicodedata
from optparse import OptionParser
import subprocess
# from loguru import logger


def load_json(uri_json):
    file = open(uri_json, 'r', encoding='utf-8-sig')
    json_obj = json.load(file)
    return json_obj

def fool_mode(json_obj, duration_news):
    interval_begin = 2
    interval_end = 2
    num = len(json_obj['resources'])
    if num==0:
        return json_obj
    frag_duration = (duration_news - interval_begin - interval_end) / num
    for index in range(num):
        interval_end = interval_begin + frag_duration
        json_obj['resources'][index]['startTime'] = interval_begin
        json_obj['resources'][index]['endTime'] = interval_end
        interval_begin = interval_end
    return json_obj

def full_mode(resource):
    ratio = resource['width'] / resource['height']
    ratio_tmp = 1920 / 1080
    if ratio > ratio_tmp:
        resource['height'] = 1920 / ratio
        resource['width'] = 1920
        resource['posx'] = 0
        resource['posy'] = (1080 - resource['height']) / 2
    else:
        resource['width'] = 1080 * ratio
        resource['height'] = 1080
        resource['posx'] = (1920 - resource['width']) / 2
        resource['posy'] = 0
    return resource

# ea ; A     ; Ambiguous  不确定
# ea ; F     ; Fullwidth  全宽
# ea ; H     ; Halfwidth  半宽
# ea ; N     ; Neutral   中性
# ea ; Na    ; Narrow    窄
# ea ; W     ; Wide     宽
def chr_width(str):
    count = 0
    for c in str:
        if (unicodedata.east_asian_width(c) in ('F','W','A')):
            count = count+2
        else:
            count = count+1
    return count

def change_font(title):
    length = chr_width(title)
    if length <= 54:
        fontSize = 44
    elif length <= 64:
        fontSize = 36
    elif length <= 72:
        fontSize = 32
    elif length <= 80:
        fontSize = 29
    elif length <= 94:
        fontSize = 25
    else:
        print("Title out of range: 47")
        fontSize = 20
    fontOffset = int(979 + (44 - fontSize) / 2)
    return fontOffset, fontSize

def change_time(t):
    return int(t*25)/25

if __name__ == "__main__":
    parser = OptionParser()
    parser.add_option("-j", "--name_json", dest="name_json", help="The name of json", action="store",
                      type="string", default="test.json")
    (opt, args) = parser.parse_args()

    # news resource (1080P 25fps)
    json_obj = load_json(os.getcwd() + "/input/" + opt.name_json)
    news_dir = "file://" + os.getcwd() + "/input/" + json_obj['id']
    news_resource = ffmpeg.input(news_dir)
    news_duration = float(ffmpeg.probe(news_dir)['streams'][0]['duration'])
    print("news duration: '%s'" % news_duration)

    # head resource (1080P 25fps)
    if json_obj['head']['name']:
        head_dir = "file://" + os.getcwd() + "/images/" + json_obj['head']['name']
        head_duration = float(ffmpeg.probe(head_dir + ".mp4")['streams'][0]['duration'])
        audio_duration = float(ffmpeg.probe(head_dir + ".mp3")['streams'][0]['duration'])
        head_video = ffmpeg.input(head_dir + ".mp4")
        head_audio = ffmpeg.input(head_dir + ".mp3")
        if json_obj['head']['audioModel'] == 'trim':
            # news_audio = news_resource.audio.filter('atrim', start=audio_duration-head_duration, end=news_duration)
            news_audio = news_resource.audio.filter('atrim', start=audio_duration - head_duration + 0.3, end=news_duration)

            v_file = ffmpeg.concat(head_video.video, news_resource.video, n=2, a=0, v=1).node[0]
            a_file = ffmpeg.concat(head_audio, news_audio, n=2, a=1, v=0).node[0]
            joined = ffmpeg.concat(v_file, a_file, n=1, a=1, v=1).node

            news_resource = joined[0]
            audio = joined[1]
        else:
            # news_audio = news_resource.audio.filter('adelay', delays=head_duration*1000)
            news_audio = ffmpeg.concat(head_video.audio, news_resource.audio, n=2, a=1, v=0).node[0]

            v_file = ffmpeg.concat(head_video.video, news_resource.video, n=2, a=0, v=1).node[0]
            a_file = ffmpeg.filter([head_audio, news_audio], 'amix', duration='longest')
            joined = ffmpeg.concat(v_file, a_file, n=1, a=1, v=1).node

            news_resource = joined[0]
            audio = joined[1]

    else:
        head_duration = 0
        audio = news_resource.audio
        news_resource = news_resource.video

    # fool model
    if json_obj['interval'] == "fool":
        json_obj = fool_mode(json_obj, news_duration)

    # resources overlay
    popup_list = []
    bg_dir = "file://" + os.getcwd() + "/images/background.jpg"
    for resource in json_obj['resources']:
        resource['startTime'] = change_time(resource['startTime'])
        resource['endTime'] = change_time(resource['endTime'])

        # if resource['type'] == "video" and resource['model'] == "full":
        #     # resize full video and add background
        #     resource = full_mode(resource)
        #     bg_dir = "file://" + os.getcwd() + "/images/background.png"
        #     news_resource = ffmpeg.filter([news_resource, ffmpeg.input(bg_dir).filter('scale', w=1920, h=1080)], 'overlay',
        #         enable='between(t,' + str(resource['startTime'] + head_duration) + ',' + str(resource['endTime'] + head_duration) + ')')

            # ffmpeg.output(float_window, 'output/test.mp4').run(overwrite_output=True)
            # os._exit()

        if resource['popup']['flag'] == True:
            # add background image, while popup model
            news_resource = ffmpeg.filter([news_resource, ffmpeg.input(bg_dir)], 'overlay',
                enable='between(t,' + str(resource['startTime'] + head_duration) + ',' + str(resource['endTime'] + head_duration) + ')')

        print("resource info: '%s'" % resource)
        resource_dir = "file://" + os.getcwd() + "/input/" + resource['name']
        overlay_resource = ffmpeg.input(resource_dir, itsoffset=resource['startTime'] + head_duration, t=resource['endTime']-resource['startTime'])

        if resource['type'] == "gif":
            overlay_resource = ffmpeg.filter(overlay_resource, 'loop', loop=-1, size=32767)
            news_resource = ffmpeg.filter(
                [news_resource, overlay_resource.filter('scale', w=resource['width'], h=resource['height'])],
                'overlay', x=resource['posx'], y=resource['posy'], shortest=True,
                enable='between(t,' + str(resource['startTime'] + head_duration) + ',' + str(
                    resource['endTime'] + head_duration) + ')')
        else:
            news_resource = ffmpeg.filter([news_resource, overlay_resource.filter('scale', w=resource['width'], h=resource['height'])],
                                      'overlay', x=resource['posx'], y=resource['posy'],
            enable='between(t,'+str(resource['startTime']+head_duration)+','+str(resource['endTime']+head_duration)+')')
        print("@@@", news_resource)
        # add popup resources
        if resource['popup']['flag'] == True:
            popup = ffmpeg.input(news_dir, itsoffset=head_duration).filter('crop', x=resource['popup']['cropx'],y=resource['popup']['cropy'],
                                 w=resource['popup']['cropw'],h=resource['popup']['croph']).filter('scale',w=resource['popup']['width'],h=resource['popup']['height'])
            print("###", popup)
            resource['popup_resource'] = popup
            popup_list.append(resource)
            # news_resource = ffmpeg.filter([news_resource, popup], 'overlay', x=1600, y=630,
            #                               enable='between(t,' + str(resource['startTime'] + head_duration) + ',' + str(
            #                                   resource['endTime'] + head_duration) + ')')


    # text overlay
    if json_obj['text_overlay'] == True:
        logo_dir = "file://" + os.getcwd() + "/images/logo.png"
        logo_resource = ffmpeg.input(logo_dir).filter('scale', w=1920, h=1080)
        news_resource = ffmpeg.filter([news_resource, logo_resource], 'overlay',
            enable='between(t,' + str(head_duration+2) + ',' + str(head_duration+news_duration) + ')')

        pfdn_dir = "file://" + os.getcwd() + "/images/pfdn05.mov"
        pfdn_resource = ffmpeg.input(pfdn_dir, itsoffset=head_duration).filter('scale', w=1920, h=1080)
        news_resource = ffmpeg.filter([news_resource, pfdn_resource], 'overlay',
            enable='between(t,' + str(head_duration) + ',' + str(head_duration+news_duration) + ')')

        news_resource = ffmpeg.filter(news_resource, 'drawtext', fontfile='noto/simhei.ttf', text=json_obj['pre_title'],
                                      x=270, y=976, fontcolor='white', fontsize=48,
                                      enable='between(t,' + str(head_duration+1) + ',' + str(head_duration+news_duration) + ')')
        [pos_y, fontsize] = change_font(json_obj['title'])
        news_resource = ffmpeg.filter(news_resource, 'drawtext', fontfile='noto/simhei.ttf', text=json_obj['title'],
                                      x=540, y=pos_y, fontcolor='white', fontsize=fontsize,
                                      enable='between(t,' + str(head_duration+1) + ',' + str(head_duration+news_duration) + ')')

    # popup overlay
    for resource in popup_list:
        news_resource = ffmpeg.filter([news_resource, resource['popup_resource']], 'overlay', x=resource['popup']['posx'],
                                      y=resource['popup']['posy'],enable='between(t,' + str(resource['startTime'] + head_duration) +
                                      ',' + str(resource['endTime'] + head_duration) + ')')

    news_resource = news_resource.filter('scale', w=json_obj['output']['pixel'][0], h=json_obj['output']['pixel'][1])

    ###live on test###
    # ffmpeg.output(news_resource, audio, 'rtmp://0.0.0.0:9999/live/test', ac=1, ar=44100, strict=-2, f='flv', acodec='aac').run()
    ###test over###

    # output config (mp4 avi flv mov webm ts)
    file_name = json_obj['output']['name']+'.'+json_obj['output']['format']
    file_mp4 = json_obj['output']['name']+'.mp4'
    # flv format need ar=44100
    if json_obj['output']['format'] == 'flv':
        ffmpeg.output(news_resource, audio, 'output/pre_' + file_mp4, ac=2, ar=44100, metadata="title=" + json_obj['title']).run(overwrite_output=True)
    else:
        ffmpeg.output(news_resource, audio, 'output/pre_'+file_mp4, ac=2, metadata="title="+json_obj['title']).run(overwrite_output=True)

    # webm format speed up
    if json_obj['output']['format'] == 'webm':
        cmd = "ffmpeg -threads 4 -i %s -vcodec libvpx -cpu-used -1 -deadline realtime -b:v 1M %s -y" % ('output/pre_' + file_mp4, 'output/pre_' + file_name)
        subprocess.call(cmd, shell=True)
        cmd = "rm %s" % ('output/pre_' + file_mp4)
        subprocess.call(cmd, shell=True)
    elif json_obj['output']['format'] != 'mp4':
        cmd = "ffmpeg -threads 4 -i %s -c copy %s -y" % ('output/pre_' + file_mp4, 'output/pre_' + file_name)
        subprocess.call(cmd, shell=True)
        cmd = "rm %s" % ('output/pre_' + file_mp4)
        subprocess.call(cmd, shell=True)

    # add metadata
    cmd = "ffmpeg -i %s -metadata author=%s -metadata copyright=%s -metadata comment=%s -c copy -movflags faststart %s -y" % \
          ('output/pre_'+file_name, "浦发银行", "\"Spdb, all rights preserved\"", "浦发数字主播", 'output/'+file_name)
    subprocess.call(cmd, shell=True)
    cmd = "ffmpeg -ss %s -i %s -vframes 1 %s -y" % (head_duration+1.5, 'output/'+file_name, 'output/'+json_obj['output']['name']+'.jpg')
    subprocess.call(cmd, shell=True)
    cmd = "rm %s" % ('output/pre_'+file_name)
    subprocess.call(cmd, shell=True)
